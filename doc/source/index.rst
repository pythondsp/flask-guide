.. Flask Guide documentation master file, created by
   sphinx-quickstart on Thu Feb  1 18:10:16 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Flask Guide's documentation!
=======================================

.. toctree::
   :maxdepth: 2
   :numbered:
   :caption: Contents:

   flask/flask
   flask/flask2